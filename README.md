# README #

> Please, read this carefully, and never and ever push data here. Use the GoogleDrive for data.

> Also, compress the data with *.tar.gz before pushing to Drive unless it is few files.

### What is this repository for? ###

This repo contains all the code that we develop for the **National Data Bowl at Kaggle. ** 

Please, separate code in to a nice folder structure. Preferably by model algorithm or programming language or functionality. 

For instance, all `Matlab`, `python` and `cpp` code should go to their respective folders.

Similarly all data `pre-processing` and `model` building `algorithms` should go to their own separate folders. 

Use your judgement to make the repository cleaner and easy to navigate.

    v0.0.0pre

### Where is STUFF? ###

* cxxnet-master has the latest copy with configuration that I am currently using on my laptop
* cxxnet contains a little older version and will be removed soon
* theano contains the theano-based CNN code
* preprocess contains the preprocessing including offline and online data augmentation

### How do I get set up? ###

* Will update the readme of cxxnet-master with dependencies and steps to follow to build it.
* cxxnet has two config files you should adjust: bowl.conf (training) and pred.conf (for testing)
* Dependencies
* data preprocessing
* How to run tests
***
### Contribution guidelines ###

* Writing tests: Code need to be tested well including edge cases, before pushing here. Don't waste the time of others that try to use your code. Your code should work out of the box with minimal configuration changes such as directories and some config parameters.
* Code review: each of us should randomly test code that gets pushed here and report bugs.
* Other guidelines: If you have more guidelines that you want others to follow, write them here.

For now, this is sufficient :)