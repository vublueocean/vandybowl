# Caffe

## Net Definition
The network is defined in `cxxnet_like_trainer.prototxt` and is based entirely off [antinucleon's kaggle net](https://github.com/antinucleon/cxxnet/blob/master/example/kaggle_bowl/bowl.conf).

## Data Definition
The data for the CNN is to be stored in a [LMDB](http://symas.com/mdb/) file as Caffe provides a script to auto create these files, they are fast, and the examples I have seen use them. The script source can be found at the [github page](https://github.com/BVLC/caffe/blob/master/tools/convert_imageset.cpp).

In order to run the script, you need a file containing all images and their classification as follows:

``
subfolder1/i_am_a_plankton.jpeg 1
``

where 1 is the class for the given plankton. This is called a *list file*.

### Creating the list files
I created a python script to auto generate these called `create_list_files.py`. This allows the user to generate these list files with

``
python2 create_list_files.py <root_node_dir> <root_image_dir> <class_names_file>
``

This will automatically create all the list files needed for each node of the learning tree.

### Creating the lmdb file

After we create this list file, we can create the lmdb file for Caffe to use by running the following:

``
convert_imageset <root_folder> <list_file> <db_name> -gray
``

## Running the net

Update the prototxt file to the new database filename.

Finally, the CNN should be ready to be trained with 
``
caffe train --solver=learner.prototxt
``

## Running the net on specific nodes

Change the *source* value for the *plankton_data* layer in `cxxnet_like_trainer.prototxt`:

``
layer {
    name: "plankton_data"
    type: "Data"
    data_param {
        source: "./path/to/lmdb/file"
        backend: LMDB
        batch_size: 256
        scale: 0.00390625
    }
    top: "data"
    top: "label"
}
``

To train the net on the entire data set (without any tree structure), use the node `AllData.yaml`.
