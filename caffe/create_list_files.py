#! /usr/bin/python2

"""
This file will create a "filelist" file for creating Caffe data blobs.

This file will need to:
    + get all image file names
    + take a dictionary of class groupings
"""

import os
import sys
import yaml

if len(sys.argv) < 3:
    print("Usage: python2 create_list_files.py <root_node_dir> <root_image_dir> <class_names_file>");
    exit(1);

original_class_name_file = "class_names.txt";

if len(sys.argv) > 3:
    original_class_name_file = sys.argv[3];

root_image_dir = sys.argv[2];
root_node_dir = sys.argv[1];

def load_all_class_names(filename):
    data = open(filename, 'r');
    data.seek(0);
    class_names = [];
    for line in data:
        class_names.append(line.replace('\n',''));
    return class_names;

def create_original_to_actual_class_map(node_file_name, original_class_names):
    original_to_actual = {}; 
    with open(node_file_name, 'r') as f:
        node = yaml.load(f);

    branches = node.keys()
    branches.sort();  # Classes are indexed alphabetically by branch name
    for i in range(len(branches)):
        classes = node[branches[i]]
        for image_class in classes:
            class_name = original_class_names[image_class];
            original_to_actual[class_name] = i;
    return original_to_actual;

def create_list_file_name(node):
    node = node.replace('.yaml','_');
    return node+'list_file';


def write_list_file(filename, image_dir, class_dict):
    output_file = open(filename, 'w');
    for image_class in class_dict:
        images = os.listdir(os.path.join(image_dir,image_class));
        for image in images:
            output_file.write(os.path.join(image_class,image)+' '+str(class_dict[image_class])+'\n');

    output_file.close();

def create_list_file(class_file, node_file, out_file, root_dir):
    original_class_names = load_all_class_names(class_file);
    original_to_actual = create_original_to_actual_class_map(node_file, original_class_names);
    write_list_file(out_file, root_dir, original_to_actual);

# Get all node file names
nodes = os.listdir(root_node_dir);
for node in nodes:
    node_file_name = os.path.join(root_node_dir, node);
    output_file = create_list_file_name(node);
    create_list_file(original_class_name_file, node_file_name, output_file, root_image_dir)

print('List files created!');
print('Create lmdb data file for Caffe with\n\n\tconvert_imageset '+
        root_image_dir+ ' ' + output_file + ' <db_name>\n');
