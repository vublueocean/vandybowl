"""
Given a directory of list_files, we will create the lmdb files for them

We will create the list files for the node directory
"""

import os
import sys
from os.path import join

if len(sys.argv) < 4:
    print "Usage: python create_lmdb_from_list_files <list_dir> <image_root> <output_dir>"
    exit(1)

# Create the lmdb file from the list_file
# run the convert imageset command
def create_lmdb(image_root, list_file, output_name):
    cmd = "convert_imageset -gray "+join(image_root,"")+" "+list_file+" "+output_name;
    print('Calling '+cmd);
    os.system(cmd);

list_dir = os.path.abspath(sys.argv[1])
image_dir = os.path.abspath(sys.argv[2])
output_dir = os.path.abspath(sys.argv[3])

list_files = os.listdir(list_dir)

try:
    os.mkdir(output_dir)
except:
    pass

for lf in list_files:
    # Get the output_name
    name = join(output_dir, lf.replace('list_file', 'lmdb'));
    create_lmdb(image_dir, join(list_dir, lf), name)



