import os
import sys
from os.path import join
from multiprocessing import Pool

if len(sys.argv) < 3:
    print "Usage: python gen_test.py input_folder output_folder"
    exit(1)

fi = os.path.abspath(sys.argv[1])
fo = os.path.abspath(sys.argv[2])

#cmd = "convert -resize 96x96\! "
pixels=96
cmd = "convert -resize {}x{} -background white -compose Copy -gravity center -extent {}x{} ".format(pixels,pixels,pixels,pixels)
tasks = os.listdir(fi)

if not os.path.exists(fo):
    os.mkdir(fo)
print "Creating directories/Generating tasks"

total = len(imgs)
def resize(img):
    #print img
    md = ""
    md += cmd
    md += join(fi, img)
    md += " " + join(fo, img)
    os.system(md)
    #print "{:.2f}%    \r".format((i*100.0)/total),

if __name__ == '__main__':
    p = Pool(5)
    for i, _ in enumerate(p.imap_unordered(resize, tasks), 1):
        print "{} {:.2f}%    \r".format(i, (i*100.0)/total),
    print
    print "Done"


