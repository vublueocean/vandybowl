import os
import sys
from os.path import join
from multiprocessing import Pool
from itertools import izip
import glob

if len(sys.argv) < 3:
    print "Usage: python gen_train.py input_folder output_folder"
    exit(1)

fi = os.path.abspath(sys.argv[1])
fo = os.path.abspath(sys.argv[2])

pixels=96
cmd = "convert -resize {}x{} -background white -compose Copy -gravity center -extent {}x{} ".format(pixels,pixels,pixels,pixels)
classes = os.listdir(fi)

if not os.path.exists(fo):
    os.mkdir(fo)

print "Creating directories/Generating tasks"
tasks = []
for cls in classes:
    cls_out_dir = join(fo,cls)
    if not os.path.exists(cls_out_dir):
        os.mkdir(cls_out_dir)

    imgs = os.listdir(join(fi, cls))
    for img in imgs:
        tasks.append((cls,img))

print "{} tasks".format(len(tasks))

import time
def resize((cls, img)):
    #print "Processing: ", cls
    md = ""
    md += cmd
    md += join(fi, cls, img)
    md += " " + join(fo, cls, img)

    os.system(md)

total = len(tasks)

p = Pool(8)
for i, _ in enumerate(p.imap_unordered(resize, tasks,10), 1):
    print "{} {:.2f}%    \r".format(i, (i*100.0)/total),
