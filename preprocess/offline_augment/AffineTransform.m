function J = AffineTransform(I, t, r, ref, sh, sc)
% This function performs arbitrary affine transformation using the given
% scale - sc, rotation - r (rad), shear - sh (pix), translation - t (pix).
% all of these areguments can be 1x2 or 2x1 with first one x-comp

% First, handle the input arguments and set defaults
transOnly = 0;
shearOnly = 0;
if(nargin <= 1 || isempty(t))
    t = [0 0];
end

if(nargin <= 2 || isempty(r))
    if(nargin == 2)
        transOnly = 1; 
    end
    r = 0;
end
if(nargin <= 3 || isempty(ref))
    ref = [1 1];
end
if(nargin <= 4 || isempty(sh))
    sh = [1 1];
end
if(nargin <= 5 || isempty(sc))
    if(nargin==5)
        shearOnly = 1;
    end
    sc = 1;
end


% create the transform matrix
shx = sh(1); shy = sh(2);
tx = t(1); ty = t(2);
refx = ref(1); refy = ref(2);

T = [refx*sc*cos(r) sc*sin(r) 0;
    -sc*sin(r) refy*sc*cos(r) 0;
                tx  ty        1];
if(shearOnly)
    T = [1 shy 0;
        shx 1  0;
         0  0  1];
end
       
Tx = affine2d(T);
if(transOnly)
    Ro = imref2d(size(I));
    try
    Ro.XWorldLimits(2) = Ro.XWorldLimits(2) + tx;
    Ro.YWorldLimits(2) = Ro.YWorldLimits(2) + ty;
    
    J = imwarp(I,Tx,'OutputView',Ro,'FillValues',255);
    catch err
        disp(Ro);
        disp(t);
        J=I;
        rethrow(err);
    end
else
    J = imwarp(I,Tx,'FillValues',255);
end
% Plot the result
%figure,imshow(J)
