function AugmentAllTrainData(src, Tx, N, K, mode)
%clear all 
close all
clc
if(nargin < 1 || isempty(src))
    %Set this string to the folder name of the original training data
    src = 'F:/bowl/data/train_96x96'; %'Images';
elseif(nargin < 2 || isempty(Tx))
    Tx = {'rot'};
elseif(nargin < 3 || isempty(N))
    N = 10;
end

dst = strcat(src, '_', strcat(Tx{:}));
%Set this string to the folder name of what you want to call the generated
%data
originalDataFolder = src;
generatedDataFolder = dst;
% If dest folder exists remove it
if(exist(generatedDataFolder, 'file'))
  [~, ~, ~] = rmdir(generatedDataFolder,'s');
  rehash();
end
% Create dest folder
mkdir(generatedDataFolder);
% Get the image classes from internal folders
dirStruct = dir(originalDataFolder);
imageClasses = {dirStruct.name}.';
imageClasses = imageClasses(3:end);

%Delete any Thumbs file there
for i=1:length(imageClasses)
 currentDir = [originalDataFolder '/' imageClasses{i}];
 if(exist([currentDir '/' 'Thumbs.db'],'file'))
    delete([currentDir '/' 'Thumbs.db']);
 end
end

%Generate Images
% The train/test images end at 160,736 = 30,336 (orig train) + 130,400 (test)
% count = 160737; 
Lk = length(imageClasses);
% For each folder
parfor i=1:Lk  % For each directory
  
  currentClass = imageClasses{i};
  disp(['Processing class: ' currentClass '(' int2str(i) ' of ' int2str(Lk) ')'])
  currentDirectoryOriginal = [originalDataFolder '/' currentClass];
  currentDirectoryGenerated = [generatedDataFolder '/' currentClass]; 
  mkdir(currentDirectoryGenerated);

  dirStruct = dir(currentDirectoryOriginal);
  imageList = {dirStruct.name}.';
  imageList = imageList(3:end);
  
  % Copy the original images first.
  copyfile(currentDirectoryOriginal,currentDirectoryGenerated);
  M = length(imageList);
  % For each image in the current folder, Generate N new images using the
  % method transform Tx
  theta = 0; shx=0; shy =0; scx =1; scy=1; tx = 0; ty =0;
  refx = 1; refy = 1;
  for j=1:M % for each image
    originalImage = imread([currentDirectoryOriginal '/' imageList{j}]);
    ii = 1;
    fl = 1;
    flp = [1 -1;-1 1; -1 -1];
    for n=1:N % for each new image to generate
        comb = Tx{ii};               
        switch(comb)
            case 'rot'
                theta = (2*rand-1) * pi; % [-pi,+pi]
            case 'shear'
                shx = (2*rand-1) * .5
                shy = (2*rand-1) * .5; 
            case 'zoom'
                scx = 1 + (2*rand-1) * .5;
                scy = 1 + (2*rand-1) * .5;
            case 'tran'
                tx = (2*rand-1) * (.5 * size(originalImage,1));
                ty = (2*rand-1) * (.5 * size(originalImage,2));
            case 'flip'
                % get randomly either 1 or -1 for each but not 1 for both 
                temp = flp(fl,:);
                refx = temp(1);
                refy = temp(2);
                fl = fl + 1;
                if(fl>3)
                    fl = 1;
                end
        end   
        if(strcmp(mode,'each'))
            transformedImage = transformImage(originalImage, ...
            shx, shy, scx, scy, ...
            theta, tx, ty, refx, refy);

            imwrite(transformedImage, [currentDirectoryGenerated '/' strrep(imageList{j},'.jpg','') '_' comb int2str(n) '.jpg']);
            disp(['Generated Image Number: ' int2str(n) ' of ' int2str(N) ' of ' imageList{j} ' with transforms: ' strcat(comb)]);
            disp(['with theta: ' num2str(theta*180/pi)])
            disp(['(shx,shy): (' num2str(shx) ',' num2str(shy) ') ']);
            disp(['(scx,scy): (' num2str(scx) ',' num2str(scy) ') ']);
            disp(['(tx,ty): (' num2str(tx) ',' num2str(ty) ') ']);
        end            
        if(mod(n,N/K)==0)
                ii = ii + 1;
        end
%         if(strcmp(mode,'all'))
%             transformedImage = transformImage(originalImage, ...
%             shx, shy, scx, scy, ...
%             theta, tx, ty, refx, refy);
% 
%             imwrite(transformedImage, [currentDirectoryGenerated '/' strrep(imageList{j},'.jpg','') '_' int2str(n) '.jpg']);
%             disp(['Generated Image Number: ' int2str(n) ' of ' int2str(N) ' of ' imageList{j}]);
%             disp(['with theta: ' num2str(theta*180/pi)])
%             disp(['(shx,shy): (' num2str(shx) ',' num2str(shy) ') ']);
%             disp(['(scx,scy): (' num2str(scx) ',' num2str(scy) ') ']);
%             disp(['(tx,ty): (' num2str(tx) ',' num2str(ty) ') ']);
%         end
    end
  end
end