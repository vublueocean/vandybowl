clear all 
close all
clc

%Change this variable to select how many new images will be generated
totalNumberOfNewImages = 100000;
wt = 1.0; % warping threshold, was 1.3
sigma = 0.1;

%if 0, this will generate images in the same proportion as the original
%dataset
%else it will generate images so that each class has the same number of
%images
makeUniform = 0;

%Set this string to the folder name of the original training data
originalDataFolder = 'F:/bowl/data/train_96x96'; %'Images';
%Set this string to the folder name of what you want to call the generated
%data
generatedDataFolder = 'F:/bowl/data/train_96x96_rot';

%files = [];

if(exist(generatedDataFolder, 'file'))
  [a, b, c] = rmdir(generatedDataFolder,'s');
  rehash();
end
mkdir(generatedDataFolder);

dirStruct = dir(originalDataFolder);
imageClasses = {dirStruct.name}.';
imageClasses = imageClasses(3:end);

%Find the original population sizes of the training set
originalPopulationSize = zeros(length(imageClasses),1);
for i=1:length(imageClasses)
 currentDir = [originalDataFolder '/' imageClasses{i}];
 if(exist([currentDir '/' 'Thumbs.db'],'file'))
    delete([currentDir '/' 'Thumbs.db']);
 end
 dirStruct = dir(currentDir);
 imageList = {dirStruct.name}.';
 imageList = imageList(3:end);
 %fileList = cell2mat(cellfun(@str2num,strrep(imageList,'.jpg',''), 'UniformOutput',false));
 %files = [files;fileList];
 originalPopulationSize(i) = size(imageList,1);
end

%Figure out how many to make for each class
numberOfImagesToGenerate = originalPopulationSize;
if(makeUniform == 0)
  %Create images so that the distribution of the generated images matches the distribution of images from the original dataset
  totalOriginalPopulation = sum(originalPopulationSize);
  numberOfImagesToGenerate = numberOfImagesToGenerate ./ totalOriginalPopulation;
  numberOfImagesToGenerate = round(numberOfImagesToGenerate .* totalNumberOfNewImages); 
else
  %Create images so that each class has roughly the same number of (original+generated) images
  numberForEachClass = round((sum(originalPopulationSize) + totalNumberOfNewImages) / length(imageClasses));
  numberOfImagesToGenerate = numberForEachClass - numberOfImagesToGenerate;
  numberOfImagesToGenerate(numberOfImagesToGenerate < 0) = 0;
end

%Generate Images
% The train/test images end at 160,736 = 30,336 (orig train) + 130,400 (test)
count = 160737; 
for i=1:length(numberOfImagesToGenerate)
  
  currentClass = imageClasses{i};
  disp(['Processing class: ' currentClass '(' int2str(i) ' of ' int2str(length(numberOfImagesToGenerate)) ')'])
  currentDirectoryOriginal = [originalDataFolder '/' currentClass];
  currentDirectoryGenerated = [generatedDataFolder '/' currentClass]; 
  mkdir(currentDirectoryGenerated);

  dirStruct = dir(currentDirectoryOriginal);
  imageList = {dirStruct.name}.';
  imageList = imageList(3:end);
  
  % Copy the original images together with the newly generated ones.
  copyfile(currentDirectoryOriginal,currentDirectoryGenerated);
  
  for j=1:numberOfImagesToGenerate(i)
    %Select a random image from the original data
    selectedIndex = mod(j,size(imageList,1))+1; %randi(size(imageList,1));
    originalImage = imread([currentDirectoryOriginal '/' imageList{selectedIndex}]);
    %transformedImage = randomTransformation(originalImage, sigma, wt);
    %transformedImage = RandomAffineTransform(originalImage);
    theta = rand * 2 * pi;    
    transformedImage = transformImage(originalImage, ...
    0, 0, 1, 1, ...
    theta, 0, 0);

    imwrite(transformedImage, [currentDirectoryGenerated '/' num2str(count) '.jpg']);
    count = count + 1;
    disp(['Generated Image Number: ' int2str(j) ' of ' int2str(numberOfImagesToGenerate(i))...
         ' with theta: ' num2str(theta*180/pi)])
    %disp(['The count is: ' num2str(count)])
    %['Image Number: ' int2str(j) 'of' int2str(numberOfImagesToGenerate(i))]
  end

end