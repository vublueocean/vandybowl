clear all 
close all
clc

%Change this variable to select how many new images will be generated
totalNumberOfNewImages = 100000;

%if 0, this will generate images in the same proportion as the original
%dataset
%else it will generate images so that each class has the same number of
%images
makeUniform = 1;

%Set this string to the folder name of the original training data
originalDataFolder = 'Images';
%Set this string to the folder name of what you want to call the generated
%data
generatedDataFolder = 'GeneratedData';

if(exist(generatedDataFolder, 'file'))
  [a b c] = rmdir(generatedDataFolder,'s');
  rehash();
end
mkdir(generatedDataFolder);

dirStruct = dir(originalDataFolder);
imageClasses = {dirStruct.name}.';
imageClasses = imageClasses(3:end);

%Find the original population sizes of the training set
originalPopulationSize = zeros(length(imageClasses),1);
for i=1:length(imageClasses)
 currentDir = [originalDataFolder '/' imageClasses{i}];
 if(exist([currentDir '/' 'Thumbs.db'],'file'))
    delete([currentDir '/' 'Thumbs.db']);
 end
 dirStruct = dir(currentDir);
 imageList = {dirStruct.name}.';
 imageList = imageList(3:end);
 originalPopulationSize(i) = size(imageList,1);
end

%Figure out how many to make for each class
numberOfImagesToGenerate = originalPopulationSize;
if(makeUniform == 0)
  %Create images so that the distribution of the generated images matches the distribution of images from the original dataset
  totalOriginalPopulation = sum(originalPopulationSize);
  numberOfImagesToGenerate = numberOfImagesToGenerate ./ totalOriginalPopulation;
  numberOfImagesToGenerate = round(numberOfImagesToGenerate .* totalNumberOfNewImages); 
else
  %Create images so that each class has roughly the same number of (original+generated) images
  numberForEachClass = round((sum(originalPopulationSize) + totalNumberOfNewImages) / length(imageClasses));
  numberOfImagesToGenerate = numberForEachClass - numberOfImagesToGenerate;
  numberOfImagesToGenerate(numberOfImagesToGenerate < 0) = 0;
end

%Generate Images
parfor i=1:length(numberOfImagesToGenerate)
  ['Class Number: ' int2str(i) 'of' int2str(length(numberOfImagesToGenerate))]
  currentClass = imageClasses{i};
  currentDirectoryOriginal = [originalDataFolder '/' currentClass];
  currentDirectoryGenerated = [generatedDataFolder '/' currentClass]; 
  mkdir(currentDirectoryGenerated);

  dirStruct = dir(currentDirectoryOriginal);
  imageList = {dirStruct.name}.';
  imageList = imageList(3:end);
  for j=1:numberOfImagesToGenerate(i)
    %Select a random image from the original data
    selectedIndex = mod(j,size(imageList,1))+1;%randi(size(imageList,1));
    originalImage = imread([currentDirectoryOriginal '/' imageList{selectedIndex}]);
    transformedImage = transformImage(originalImage, randn * .2, randn * .2, 1 + randn *.2, 1+ randn * .2, rand * 2 * pi, rand * (.2 * size(originalImage,1)), rand * (.2 * size(originalImage,2)));
    imwrite(transformedImage, [currentDirectoryGenerated '/' 'Generated' int2str(j) '.jpg']);
    %['Image Number: ' int2str(j) 'of' int2str(numberOfImagesToGenerate(i))]
  end

end









