function J = RandomAffineTransform(I)
% This function creates a random affine transform by first selecting
% randomly what kind of transform it should perform and then slecting
% random reasonable values for the selected transform

% pick randomly from rot, tran, ref (mirror) and a combination of them
% TODO: in the future, may be do scaling as well as combination
% 1=trans, 2=rot, 3=ref, 4=shear, 5=scale
Tx = randi(4); % not including scaling
% Now, based on the randomly picked Tx, 
% populate random parameters for each kind of Tx

% Translation
t=round(0.2*[2*rand()-1,2*rand()-1].*flip(size(I)));
% Rotation
r = (2*rand()-1)*pi;
% reflection/flip
ref = [1,1];
while ref==[1,1]
    ref = [(2*(rand()>=0.5)-1),(2*(rand()<0.5)-1)];
end
% Shear
sh = 0.2*[(2*rand()-1),(2*rand()-1)];

switch Tx
    case 1
        J = AffineTransform(I,t);
    case 2
        J = AffineTransform(I,[],r);
    case 3        
        J = AffineTransform(I,[],[],ref);
    case 4
        J = AffineTransform(I,[],[],[],sh);
    otherwise
        % TODO: implement combination or scaling
end