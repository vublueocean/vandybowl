function performOfflineDataAugmentation(mode)
% This function performs offline data augmentation using all the possible
% combination of the five transforms: rot, tran, ref, scale, and shear
% If mode == 'each' or empty performs sequential augmenting of N new images for each
% transform in Tx for each image, else if it is 'all' it applies all the transforms
% in Tx and generates just N new images for each image
close all
clc
if(nargin<1 || isempty(mode))
    mode = 'each';
end
% Enumerate all the combination
Tx = {'tran', 'flip', 'zoom', 'rot', 'shear'};
L = length(Tx);
N = [3,6,6,8,5]; % this applies 10 new images for each training image => 300,000 train images

for i =1:L
    comb = nchoosek(Tx,i);
    display(comb);
    [Lc,K] = size(comb);
    for j=1:Lc
        J = N(i); 
        display(['The number of new images is: ' num2str(J)]);
        display(['Processing: ' comb(j,:)]);
        AugmentAllTrainData('F:/kaggle/data/bowl/train_96x96', comb(j,:), J, K, mode);
    end
end


