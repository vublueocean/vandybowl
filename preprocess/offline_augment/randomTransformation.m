function transformedImage = randomTransformation(inputImage, volumeSigma, warpingThreshold)

%volumeSigma: the determinant of the matrix can be thought of the "scaling
%factor of the resulting transformation with a determinant of 1
%representing a volume perserving transformation.  Thefore we randomly
%select a volume from a normal distribution centered at 1 with a sigma
%specified by volumeSigma


%warpingThreshold: in randomly generating affine matricies, there is the
%potential for the transformation to "warp" the image to become
%incredibly elongated and not really meaningful in real life.  The
%warpingThreshold serves as a check on the "elongatedness" of the
%transformation by checking the ratio of the two largest eigenvalues of the
%transformation matrix

newVolume = normrnd(1,volumeSigma);
validMatrix = 0;
TMatrix = eye(3);

while validMatrix==0  
  %Create random affine transformation
  TMatrix = [2*rand-1 2*rand-1 0;
  2*rand-1 2*rand-1 0;
  2*rand-1 2*rand-1 1];

  %scale values so that the determinant equals the new volume
  TMatrix = TMatrix.*(sqrt(newVolume) / (abs(det(TMatrix))^(1/2)));
  TMatrix(3,3) = 1;
  
  %check the eigenvalue ratio's to make sure the transformation isn't 
  %too elongated
  eigenValues = eigs(TMatrix);
  if(isreal(eigenValues(1)))
    if(abs(real(eigenValues(1)))/abs(real(eigenValues(2))) <= warpingThreshold)
      validMatrix = 1;
    end
  else
    if(abs(real(eigenValues(3)))/abs(real(eigenValues(1))) <= warpingThreshold)
      validMatrix = 1;
    end
  end
end

%transform the image
complementedImage = imcomplement(inputImage);
transformedImage = imwarp(complementedImage,affine2d(TMatrix));
transformedImage = imcomplement(transformedImage);
end

