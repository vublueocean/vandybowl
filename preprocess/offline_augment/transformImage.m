function transformedImage = transformImage(inputImage, ...
    shearX, shearY, scaleX, scaleY,...
    theta, translateX, translateY,...
    refx,refy)

shearMatX = eye(3);
shearMatY = eye(3);
shearMatX(1,2) = shearX;
shearMatY(2,1) = shearY;

scalMat = eye(3);
scalMat(1,1) = scaleX;
scalMat(2,2) = scaleY;

refMat = eye(3);
refMat(1,1) = refx;
refMat(2,2) = refy;

rotateMat = [cos(theta) sin(theta) 0; -1*sin(theta) cos(theta) 0; 0 0 1];


TMatrix = shearMatX * shearMatY * scalMat * rotateMat * refMat;

%transform the image
complementedImage = imcomplement(inputImage);
transformedImage = imwarp(complementedImage,affine2d(TMatrix));
transformedImage = imtranslate(transformedImage, [translateX, translateY], 'OutputView', 'full');
transformedImage = imcomplement(transformedImage);
end

