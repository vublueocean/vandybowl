#!/usr/bin/env python

""" CLI utility to train models and generate predictions.

Usage examples:

Training:

    ./lasagnenet.py train ../dataset/AllData_48x48_lmdb models/simple 

Prediction:

    ./lasagnenet.py predict ../dataset/AllData_48x48_lmdb models/simple --csv_file out.csv

"""
import argparse

n_classes = 121 # default number of classes

def main():
    parser = argparse.ArgumentParser(
                description="CLI utility to train and predict for kaggle ndsb")

    parser.add_argument('action', choices=['train', 'predict'], 
            help="Train model/ Run prediction")
    parser.add_argument('dataset',
            help="Dataset (lmdb database) for training/prediction")
    parser.add_argument('model_file',
            help="Model (pickle) file to be used for prediction. This is also the output file prefix when training")
    parser.add_argument('--arch', default='simple',
            help="Network architecture. Choices are [simple, cxxnet, jay, msegala] (default: simple)")
    parser.add_argument('--csv_file', help="CSV file to save prediction")
    parser.add_argument('--epochs', default=1, type=int, help="Number of epochs to train (default:1)")
    parser.add_argument('-p','--pixels', default=96, type=int, 
            help="Number if pixels in the image. Assumes square image so only one dimension is specified (default:96)")
    parser.add_argument('-c','--classes', default=n_classes, type=int, 
            help="Number of classes in the training dataset (default: {})".format(n_classes))

    args = parser.parse_args()

    if args.action == 'train':
        from neural_net_wrapper import NetModel
        model = NetModel.training(net_arch=args.arch, train_lmdb_path=args.dataset,
                num_classes=args.classes,
                num_pixels=args.pixels,
                output_prefix=args.model_file,
                num_epochs=args.epochs)

        model.train()
    elif args.action == 'predict':
        if not args.csv_file:
            print "The --csv_file argument is required for prediction"
            parser.print_usage()
            sys.exit(1)
        from neural_net_wrapper import NetModel
        model = NetModel.prediction(args.dataset)
        pred_output = model.predict(args.model_file)
        model.save_to_csv(args.csv_file, pred_output)
    else: # shouldn't get here
        print "Unknown action"
        parser.print_usage()

if __name__ == '__main__':
    main()
