import os
import glob
import numpy as np

import cPickle
import gzip
import sys

csv_header = [
 'acantharia_protist',
 'acantharia_protist_big_center',
 'acantharia_protist_halo',
 'amphipods',
 'appendicularian_fritillaridae',
 'appendicularian_s_shape',
 'appendicularian_slight_curve',
 'appendicularian_straight',
 'artifacts',
 'artifacts_edge',
 'chaetognath_non_sagitta',
 'chaetognath_other',
 'chaetognath_sagitta',
 'chordate_type1',
 'copepod_calanoid',
 'copepod_calanoid_eggs',
 'copepod_calanoid_eucalanus',
 'copepod_calanoid_flatheads',
 'copepod_calanoid_frillyAntennae',
 'copepod_calanoid_large',
 'copepod_calanoid_large_side_antennatucked',
 'copepod_calanoid_octomoms',
 'copepod_calanoid_small_longantennae',
 'copepod_cyclopoid_copilia',
 'copepod_cyclopoid_oithona',
 'copepod_cyclopoid_oithona_eggs',
 'copepod_other',
 'crustacean_other',
 'ctenophore_cestid',
 'ctenophore_cydippid_no_tentacles',
 'ctenophore_cydippid_tentacles',
 'ctenophore_lobate',
 'decapods',
 'detritus_blob',
 'detritus_filamentous',
 'detritus_other',
 'diatom_chain_string',
 'diatom_chain_tube',
 'echinoderm_larva_pluteus_brittlestar',
 'echinoderm_larva_pluteus_early',
 'echinoderm_larva_pluteus_typeC',
 'echinoderm_larva_pluteus_urchin',
 'echinoderm_larva_seastar_bipinnaria',
 'echinoderm_larva_seastar_brachiolaria',
 'echinoderm_seacucumber_auricularia_larva',
 'echinopluteus',
 'ephyra',
 'euphausiids',
 'euphausiids_young',
 'fecal_pellet',
 'fish_larvae_deep_body',
 'fish_larvae_leptocephali',
 'fish_larvae_medium_body',
 'fish_larvae_myctophids',
 'fish_larvae_thin_body',
 'fish_larvae_very_thin_body',
 'heteropod',
 'hydromedusae_aglaura',
 'hydromedusae_bell_and_tentacles',
 'hydromedusae_h15',
 'hydromedusae_haliscera',
 'hydromedusae_haliscera_small_sideview',
 'hydromedusae_liriope',
 'hydromedusae_narco_dark',
 'hydromedusae_narco_young',
 'hydromedusae_narcomedusae',
 'hydromedusae_other',
 'hydromedusae_partial_dark',
 'hydromedusae_shapeA',
 'hydromedusae_shapeA_sideview_small',
 'hydromedusae_shapeB',
 'hydromedusae_sideview_big',
 'hydromedusae_solmaris',
 'hydromedusae_solmundella',
 'hydromedusae_typeD',
 'hydromedusae_typeD_bell_and_tentacles',
 'hydromedusae_typeE',
 'hydromedusae_typeF',
 'invertebrate_larvae_other_A',
 'invertebrate_larvae_other_B',
 'jellies_tentacles',
 'polychaete',
 'protist_dark_center',
 'protist_fuzzy_olive',
 'protist_noctiluca',
 'protist_other',
 'protist_star',
 'pteropod_butterfly',
 'pteropod_theco_dev_seq',
 'pteropod_triangle',
 'radiolarian_chain',
 'radiolarian_colony',
 'shrimp-like_other',
 'shrimp_caridean',
 'shrimp_sergestidae',
 'shrimp_zoea',
 'siphonophore_calycophoran_abylidae',
 'siphonophore_calycophoran_rocketship_adult',
 'siphonophore_calycophoran_rocketship_young',
 'siphonophore_calycophoran_sphaeronectes',
 'siphonophore_calycophoran_sphaeronectes_stem',
 'siphonophore_calycophoran_sphaeronectes_young',
 'siphonophore_other_parts',
 'siphonophore_partial',
 'siphonophore_physonect',
 'siphonophore_physonect_young',
 'stomatopod',
 'tornaria_acorn_worm_larvae',
 'trichodesmium_bowtie',
 'trichodesmium_multiple',
 'trichodesmium_puff',
 'trichodesmium_tuft',
 'trochophore_larvae',
 'tunicate_doliolid',
 'tunicate_doliolid_nurse',
 'tunicate_partial',
 'tunicate_salp',
 'tunicate_salp_chains',
 'unknown_blobs_and_smudges',
 'unknown_sticks',
 'unknown_unclassified']
 
dataset_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), '../dataset'))

def get_all_images(img_path):
    """Gets a list of all images from the path

    :img_path: path to images
    :returns: TODO

    """
    return glob.glob(os.path.join(img_path,'*','*.jpg'))

def load_images(train_path, which_samples=None):
    """Load images from jpg files

    NOTE: This function should be called from an interactive session once to
    load the images into memory and pickle them

    :train_path: TODO
    :returns: TODO

    """
    from skimage.io import imread # For reading in images
    # Load the dataset

    print "Loading images..."
    all_images = get_all_images(train_path)
    all_images.sort()

    if which_samples:
        print "All images", len(all_images), "=> ", len(which_samples)
        all_images = [all_images[i] for i in which_samples]

    # Choose 80% randomly for traning, 10% for validation and 10% for test
    # Shuffle the images so we can choose randomly
    np.random.shuffle(all_images)


    # number of images to train on
    n_train = int(0.8*len(all_images))
    n_valid = int(0.1*len(all_images))

    train_images = all_images[:n_train]
    valid_images = all_images[n_train:n_train+n_valid]
    test_images = all_images[n_train+n_valid:]

    assert (len(train_images) + len(valid_images) + len(test_images) ==  len(all_images))

    image_size = 48*48
    num_features = image_size

    classes = os.listdir(train_path)
    classes.sort()

    def get_image_class(img):
        '''The class is the parent directory'''
        cls = os.path.basename(os.path.dirname(img))
        cls_num = classes.index(cls)
        #print cls_num, cls, img
        return cls_num

    def read_images(img_paths):
        n_rows = len(img_paths)
        X = np.zeros((n_rows, num_features), dtype=float)
        Y = np.zeros(n_rows)
        for i,img in enumerate(img_paths):
            cls = get_image_class(img)
            #print i, cls, img
            img_arr = imread(img, as_grey=True)
            X[i,:] = np.reshape(img_arr,(1,image_size))
            Y[i] = cls
            if i %  10 == 0:
                print i*100.0/n_rows, "%\r",
        # normalize X
        X = X/255.0

        return X,Y

    print "Loaded %d images and %d classes" %(len(all_images), len(classes))

    train_set = read_images(train_images)
    valid_set = read_images(valid_images)
    test_set = read_images(test_images)

    return train_set, valid_set, test_set
    

def pickle_and_save(data, file_path):
    """Pickle and gzip training and test data

    :data: Any data
    :file_path: path of the file to write to
    :returns: TODO

    """
    print "Pickling data to", file_path
    f = gzip.open(file_path,'wb')
    cPickle.dump(data, f)

def load_data(dataset):
    import theano
    import theano.tensor as T
    ''' Loads the dataset

    :type dataset: string
    :param dataset: the path to the dataset (here MNIST)
    '''

    print '... loading data'
    f = gzip.open(os.path.join(dataset_dir,dataset), 'rb')
    train_set, valid_set, test_set = cPickle.load(f)
    f.close()


    #train_set, valid_set, test_set format: tuple(input, target)
    #input is an np.ndarray of 2 dimensions (a matrix)
    #witch row's correspond to an example. target is a
    #np.ndarray of 1 dimensions (vector)) that have the same length as
    #the number of rows in the input. It should give the target
    #target to the example with the same index in the input.

    def shared_dataset(data_xy, borrow=True):
        """ Function that loads the dataset into shared variables

        The reason we store our dataset in shared variables is to allow
        Theano to copy it into the GPU memory (when code is run on GPU).
        Since copying data into the GPU is slow, copying a minibatch everytime
        is needed (the default behaviour if the data is not in a shared
        variable) would lead to a large decrease in performance.
        """
        data_x, data_y = data_xy
        print data_x.shape
        print data_y.shape
        shared_x = theano.shared(np.asarray(data_x,
                                               dtype=theano.config.floatX),
                                 borrow=borrow)
        shared_y = theano.shared(np.asarray(data_y,
                                               dtype=theano.config.floatX),
                                 borrow=borrow)
        # When storing data on the GPU it has to be stored as floats
        # therefore we will store the labels as ``floatX`` as well
        # (``shared_y`` does exactly that). But during our computations
        # we need them as ints (we use labels as index, and if they are
        # floats it doesn't make sense) therefore instead of returning
        # ``shared_y`` we will have to cast it to int. This little hack
        # lets ous get around this issue
        return shared_x, T.cast(shared_y, 'int32')

    test_set_x, test_set_y = shared_dataset(test_set)
    valid_set_x, valid_set_y = shared_dataset(valid_set)
    train_set_x, train_set_y = shared_dataset(train_set)

    rval = [(train_set_x, train_set_y), (valid_set_x, valid_set_y),
            (test_set_x, test_set_y)]
    return rval

def load_lmdb(lmdb_path, keys=False):
    """Load image files from lmdb database as used in caffe.

    :lmdb_path: TODO
    :returns: TODO

    """
    import lmdb
    # For some reason, if Datum is imported from the caffe package via 
    # from caffe.proto.caffe_pb2 import Datum, it takes a very long time. The
    # workaround for now is to copy the caffe_pb2 module into this directory.
    # As long as the caffe protobuf file doesn't change, this should work.
    from caffe_pb2 import Datum

    print "Opening lmdb database"
    env = lmdb.open(lmdb_path, map_size=1e9, lock=False)
    entries = env.stat()['entries']
    print "Found {} entries".format(entries)

    # Load all images and labels from database into memory
    print "Load images from database..."
    with env.begin(buffers=True) as txn:
        cursor = txn.cursor()
        cursor.first() #
        # Read the first element to get data shape
        d = Datum()
        d.ParseFromString(cursor.value())

        print "Size of images {} w x {} h".format(d.width, d.height)

        # Not sure of the order of height and width here, but height
        # corresponds to rows and width to columns so it makes sense. It also
        # means the last axis is incrementing the fastest.
        if keys:
            X = []
        else:
            X = np.zeros((entries,d.channels,d.height,d.width), dtype=np.float32)

        y = np.zeros(entries, dtype=np.uint8)

        X_tmp = np.fromstring(d.data, dtype=np.uint8).reshape(d.channels,d.height, d.width).astype(np.float32)/255.0
        if keys:
            X.append(str(cursor.key()))
        else:
            X[0,:,:,:] = X_tmp
        y[0] = d.label

        # First assignment
        X_mean = X_tmp

        def get_img_name(path):
            # Caffe puts a number at the begining of the file name so strip
            # that
            orig_path = path[path.find('_')+1:]
            #return os.path.basename(orig_path)
            return orig_path

        img_names = [get_img_name(str(cursor.key()))]

        # Go to the next element so we don't have to reparse the data
        cursor.next()

        for i, (key,value) in enumerate(cursor):
            img_names.append(get_img_name(str(key)))
            d = Datum()
            d.ParseFromString(value)

            X_tmp = np.fromstring(d.data, dtype=np.uint8).reshape(d.channels,d.height, d.width).astype(np.float32)/255.0
            if keys:
                X.append(str(key))
            else:
                X[i+1,:,:,:] = X_tmp

            X_mean += X_tmp

            y[i+1] = d.label
            print "Loading images: {:.2f}%   \r".format((i*100.0)/entries),
            sys.stdout.flush()

        X = np.array(X)
        X_mean = (X_mean/entries).astype(np.float32)

    return X,y,img_names, X_mean


def usage():
    out = '''Usage:
./%s training_dir pickle_file.pkl.gz'

Training dir should be a directory of images resized to 48x48. Each image
should be inside a directory named after its class.
    ''' % (sys.argv[0])

    return out

def load_and_pickle(argv):
    """Load and pickle images from the given training directory

    :argv: training_dir pickle_file
    :returns: TODO

    """
    if len(argv) != 3 or '-h' in argv:
        print usage()
    else:
        dataset = load_images(argv[1])
        pickle_and_save(dataset, argv[2])


if __name__ == '__main__':
    load_and_pickle(sys.argv)
