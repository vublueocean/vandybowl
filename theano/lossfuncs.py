import numpy as np
import theano.tensor as T

def logloss(x, t):
    """Calculates the log los per ndsb evaluation rules

    :x: predicted values
    :t: target values
    :returns: TODO

    """

    row_normalizer = 1/T.sum(x, axis=1)
    eps = 1e-15
    # multiply each row with its corresponding normalizer (sum of all the
    # elements in that row)
    x_tmp = x * row_normalizer[:,np.newaxis]
    x_tmp = T.clip(x_tmp,eps, 1-eps)
    lglike = T.log(x_tmp)
    # we need to select from each row of lglike the column that corresponds to
    # the target class. 
    # To do so, we create and index array such that its elements are [0, t[0],
    # 1,t[1]]. Remember that x and t are in batches so the t is of shape (batch_size,1)
    return -T.mean(T.log(x_tmp)[T.arange(t.shape[0]), t])

def multiclass_log_loss(y_true, y_pred, eps=1e-15):
    """Calculate the log loss numerically (as opposed to symbolically with
    Theano)"""
    # Not sure if we should clip before or after normalization
    predictions = np.clip(y_pred, eps, 1 - eps)
    # normalize
    predictions /= predictions.sum(axis=1)[:, np.newaxis]
    actual = np.zeros(np.shape(y_pred))
    n_samples = actual.shape[0]
    actual[np.arange(n_samples), y_true.astype(int)] = 1
    vectsum = np.sum(actual * np.log(predictions))
    loss = -1.0 / n_samples * vectsum
    return loss

def error_loss(x, t):
    """Calculate the mean of the number of missclassified labels

    :x: predicted values
    :t: target values
    :returns: error loss

    """
    # At the position indexed by t, if x is perfectly classified, there will be
    # no contribution to the error. Otherwise, the error will be proportional
    # to 1-p where p is the probablity at that index in x.
    return T.mean(1 - x[T.arange(t.shape[0]),t])

#def logloss(x, t):
    #"""Calculates the log los per ndsb evaluation rules

    #:x: predicted values
    #:t: target values
    #:returns: TODO

    #"""


    #row_normalizer = 1/T.sum(x, axis=1)
    #eps = 1e-15
    ## multiply each row with its corresponding normalizer (sum of all the
    ## elements in that row)
    #x_tmp = x * row_normalizer[:,None]
    #x_tmp = T.clip(x_tmp,eps, 1-eps)
    #return -T.mean(T.sum(t * T.log(x_tmp), axis=1))

