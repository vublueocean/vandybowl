import sys
import numpy as np
import os
import theano
import csv
import os
import gzip
import cPickle as pickle
import copy
from load_data import dataset_dir, csv_header, load_lmdb

from lasagne import layers, nonlinearities, init

from lasagne.updates import nesterov_momentum, rmsprop
from lasagne.objectives import multinomial_nll
from nolearn.lasagne import NeuralNet, BatchIterator

import lmdb
from caffe_pb2 import Datum

try:
    # use the cuda-convnet implementations of conv and max-pool layer
    from lasagne.layers import cuda_convnet
    Conv2DLayer = cuda_convnet.Conv2DCCLayer
    MaxPool2DLayer = cuda_convnet.MaxPool2DCCLayer
except:
    print "Could not use cuda_convnet. Training will be slow"
    Conv2DLayer = layers.Conv2DLayer
    MaxPool2DLayer = layers.MaxPool2DLayer

Maxout = layers.pool.FeaturePoolLayer

class NeuralNetWrapper(object):

    """This class instantiates a NeuralNet object based on provided arguments"""

    def __init__(self, 
            net_name,
            num_classes,
            output_prefix,
            train_lmdb_path,
            num_pixels=96,
            num_channels=1,
            num_epochs=50,
            **kwargs):
        """

        :net_name: Name of Neural Net configuration/architecture
        :num_classes: number of classes
        :output_prefix: The output prefix for the model to be saved
        :train_lmdb_path: Path to training lmdb database
        :num_pixels: number of pixels of the image
        :num_channels: number of channeles (3 for color, 1 for gray) of the image
        :num_epochs: Number of epochs to train
        :kwargs: Arguments to the Neuraln et

        """
        # Seed the random number generator for consistent results
        np.random.seed(0xbeef)

        self._net_arch = net_name
        self._num_classes = num_classes
        self._output_prefix = output_prefix
        self._train_lmdb_path = train_lmdb_path
        self._num_pixels = num_pixels
        self._num_channels = num_channels
        self._num_epochs = num_epochs
        self._kwargs = kwargs

        # Early stopping and model saving
        # For now, this parameters are hard coded, we can, in the future, make
        # the arguments of the class
        self._patience = 20
        self._every_n_epochs = None
        self._best_valid = np.inf
        self._best_valid_epoch = 0
        self._best_weights = None
        self._best_predict_iter = None


        self.X_mean = None # should be set when fit() is called

        self._net = self.create_net()

        self.create_dir()

    def create_dir(self):
        # get dir
        model_dir = os.path.dirname(self._output_prefix)
        try: 
            # Create the directory if it doesn't exist
            os.mkdir(model_dir)
        except:
            print "Model directory not created"
            pass

    def is_last_epoch(self, nn, epoch):
        return epoch == nn.max_epochs

    #def save_model(self, epoch, weights, train_history):
    def save_model(self,nn, train_history):
        epoch = train_history[-1]['epoch']
        last_valid = train_history[-1]['valid_loss']
        last_weights = [w.get_value() for w in nn.get_all_params()]
        file_out = "{}.pkl".format(self._output_prefix)
        output = {
                    'best_weights': self._best_weights,
                    'best_valid_loss': self._best_valid,
                    'best_epoch': self._best_valid_epoch,
                    'last_weights': last_weights,
                    'last_valid_loss': last_valid,
                    'mean_image': self.X_mean, # Should already be set
                    'epoch': epoch,
                    'best_predict_func': self._best_predict_iter,
                    'last_predict_func': self._net.predict_iter_,
                    'classes': self._num_classes,
                    'pixels': self._num_pixels,
                    'net_arch': self._net_arch,
                    'train_history': train_history
                 }

        print "Saving to", file_out
        with open(file_out,'wb') as f:
            pickle.dump(output, f, -1)

    def early_stopping(self, nn, train_history):
        """This stopping mode stops the training if the validation accuracy
        hasn't improved for a while. The parameter that controls the number of
        epochs to wait is self._patience. This method can also be configured to
        save the model every n epochs. Finally, this same method is used to
        save the best model found so far when training is finished (without
        stopping early)"""

        current_valid = train_history[-1]['valid_loss']
        current_epoch = train_history[-1]['epoch']
        if current_valid < self._best_valid:
            self._best_valid = current_valid
            self._best_valid_epoch = current_epoch
            self._best_weights = [w.get_value() for w in nn.get_all_params()]
            self._best_predict_iter = copy.deepcopy(self._net.predict_iter_)
        elif self._best_valid_epoch + self._patience < current_epoch:
            print("Early stopping.")
            print("Best valid loss was {:.6f} at epoch {}.".format(
                self._best_valid, self._best_valid_epoch))
            self.save_model(nn, train_history)

            raise StopIteration()

        if self.is_last_epoch(nn, current_epoch):
            # Save the best weights to file
            print "Finished training"
            self.save_model(nn,train_history)

        elif self._every_n_epochs and (current_epoch % self._every_n_epochs == 0):
            self.save_model(nn, train_history)

    def subtract_mean(self, X,y):
        """Subtracts mean from X. This is a tranform function to be used with a
        batch iterator"""

        # Numpy broadcasts the (1,px,py) image along the row so X - X_mean
        # works
        return X - self.X_mean, y


    def create_net(self):
        """Creates all network architectures
        
        Currently, there 4 different network architectures:

        simple: traditional neural net with one fully connected layer (used for
                testing)
        cxxnet: architecture based the CXXNET kaggle_bowl example
        jay: medium size architecture found online
        msegala: large (width) size architecture with good performance

        """

        model_saver = self.early_stopping

        # Hack to be able to subtract the mean. Doesn't lend itself for
        # subclassing
        batch_iterator=LmdbBatchIterator(batch_size=256,
                                         lmdb_path=self._train_lmdb_path,
                                         transform_cb=self.subtract_mean)

        # http://scn.sap.com/community/services/blog/2015/01/14/image-classification-with-convolutional-neural-networks-my-attempt-at-the-ndsb-kaggle-competition

        if self._net_arch == "jay":
            net = NeuralNet(
                loss = multinomial_nll,
                layers=[
                    ('input', layers.InputLayer),

                    ('conv1', Conv2DLayer),
                    ('pool1', MaxPool2DLayer),
                    ('dropout1', layers.DropoutLayer),

                    ('conv2', Conv2DLayer),
                    ('pool2', MaxPool2DLayer),
                    ('dropout2', layers.DropoutLayer),

                    ('conv3', Conv2DLayer),
                    ('pool3', MaxPool2DLayer),
                    ('dropout3', layers.DropoutLayer),

                    ('conv4', Conv2DLayer),
                    ('pool4', MaxPool2DLayer),
                    ('flatten4', layers.FlattenLayer),
                    ('dropout4', layers.DropoutLayer),

                    ('hidden5', layers.DenseLayer),
                    ('dropout5', layers.DropoutLayer),

                    ('output', layers.DenseLayer),
                    ],

                input_shape=(None, self._num_channels, self._num_pixels, self._num_pixels),
                conv1_num_filters=32, conv1_filter_size=(5, 5), conv1_border_mode='full', pool1_ds=(2, 2),
                dropout1_p=0.25,

                conv2_num_filters=64, conv2_filter_size=(5, 5), pool2_ds=(2, 2),
                dropout2_p=0.25,

                conv3_num_filters=128, conv3_filter_size=(3, 3), pool3_ds=(2, 2),
                dropout3_p=0.25,

                conv4_num_filters=256, conv4_filter_size=(3, 3), pool4_ds=(2, 2),
                dropout4_p=0.25,

                hidden5_num_units=1024,
                dropout5_p=0.75,

                output_num_units=121, 
                output_nonlinearity=nonlinearities.softmax,

                update = rmsprop,
                update_learning_rate=theano.shared(float32(0.0004)),
                #update_momentum=theano.shared(float32(0.9)),

                batch_iterator_train=batch_iterator,
                batch_iterator_test=batch_iterator,
                #batch_iterator_train=FlipBatchIterator(batch_size=128),
                #batch_iterator_train=DataAugmentationBatchIterator(batch_size=128),
                #batch_iterator_train=MirrorBatchIterator(batch_size=128),
                on_epoch_finished=[
                    AdjustVariable('update_learning_rate', start=0.0004, stop=0.00001,
                        adjust_type='expdecay'),
                    #AdjustVariable('update_momentum', start=0.9, stop=0.999),
                    model_saver,
                    ],
                max_epochs=self._num_epochs,
                verbose=1,
                eval_size=0.2
                )

        elif self._net_arch == "msegala":
            #batch_iterator = MirrorBatchIterator(batch_size=256, lmdb_path=self._train_lmdb_path)
            net = NeuralNet(
                loss = multinomial_nll,
                layers=[
                    ('input', layers.InputLayer),
                    ('conv1', Conv2DLayer),
                    ('pool1', MaxPool2DLayer),
                    ('dropout1', layers.DropoutLayer),
                    ('conv2', Conv2DLayer),
                    ('pool2', MaxPool2DLayer),
                    ('dropout2', layers.DropoutLayer),
                    ('conv3', Conv2DLayer),
                    ('pool3', MaxPool2DLayer),
                    ('dropout3', layers.DropoutLayer),
                    ('conv4', Conv2DLayer),
                    ('pool4', MaxPool2DLayer),
                    ('dropout4', layers.DropoutLayer),
                    ('hidden5', layers.DenseLayer),
                    ('dropout5', layers.DropoutLayer),
                    ('maxout5', Maxout),
                    ('hidden6', layers.DenseLayer),
                    ('dropout6', layers.DropoutLayer),
                    ('maxout6', Maxout),
                    ('output', layers.DenseLayer),
                    ],

                input_shape=(None, self._num_channels, self._num_pixels, self._num_pixels),
                conv1_num_filters=208, conv1_filter_size=(6, 6), pool1_ds=(2, 2),
                dropout1_p=0.5,
                conv2_num_filters=384, conv2_filter_size=(4, 4), pool2_ds=(2, 2),
                dropout2_p=0.2,
                conv3_num_filters=512, conv3_filter_size=(3, 3), pool3_ds=(2, 2),
                dropout3_p=0.5,
                conv4_num_filters=896, conv4_filter_size=(3, 3), pool4_ds=(2, 2),
                dropout4_p=0.2,

                #conv1_untie_biases = True,conv2_untie_biases = True,conv3_untie_biases = True,conv4_untie_biases = True,

                hidden5_num_units=8192,
                dropout5_p=0.5,
                maxout5_ds=2,
                
                hidden6_num_units=8192,
                dropout6_p=0.5,
                maxout6_ds=2,

                output_num_units=121, 
                output_nonlinearity=nonlinearities.softmax,

                update = rmsprop,
                #update_learning_rate=theano.shared(float32(0.03)),
                update_learning_rate=theano.shared(float32(0.004)), #rms prop
                #update_momentum=theano.shared(float32(0.9)),

                #batch_iterator_train=FlipBatchIterator(batch_size=128),
                #batch_iterator_train=DataAugmentationBatchIterator(batch_size=128),
                #batch_iterator_train=MirrorBatchIterator(batch_size=128),
                batch_iterator_train=batch_iterator,
                batch_iterator_test=batch_iterator,
                on_epoch_finished=[
                    #AdjustVariable('update_learning_rate', start=0.03, stop=0.0001),
                    AdjustVariable('update_learning_rate', start=0.004, stop=0.00001), #rmsprop
                    #AdjustVariable('update_momentum', start=0.9, stop=0.999),
                    model_saver,
                    ],
                max_epochs=self._num_epochs,
                verbose=1,
                eval_size=0.2,
                **self._kwargs
                )

        elif self._net_arch == "cxxnet":
            #CXXNET example
            #batch_iterator=LmdbBatchIterator(batch_size=256, lmdb_path=self._train_lmdb_path)
            net = NeuralNet(
                loss = multinomial_nll,
                layers=[
                    ('input', layers.InputLayer),
                    ('conv1', Conv2DLayer),
                    ('pool1', MaxPool2DLayer),
                    ('conv2', Conv2DLayer),
                    ('conv3', Conv2DLayer),
                    ('pool3', MaxPool2DLayer),
                    ('flatten', layers.FlattenLayer),
                    ('hidden4', layers.DenseLayer),
                    ('dropout4', layers.DropoutLayer),
                    ('hidden5', layers.DenseLayer),
                    ('dropout5', layers.DropoutLayer),
                    ('output', layers.DenseLayer),
                    ],
                input_shape=(None, self._num_channels, self._num_pixels, self._num_pixels),
                conv1_num_filters=96, conv1_filter_size=(5, 5), conv1_strides=(4,4),
                conv1_pad=2, pool1_ds=(3, 3), 
                pool1_strides=(2,2),
                conv2_num_filters=128, conv2_pad=2, conv2_filter_size=(3, 3),
                conv3_num_filters=128, conv3_pad=1,conv3_filter_size=(3, 3), pool3_ds=(3, 3),
                pool3_strides=(2,2),

                hidden4_num_units=512,
                dropout4_p=0.5,
                hidden5_num_units=512,
                dropout5_p=0.5,

                output_num_units=self._num_classes,
                output_nonlinearity = nonlinearities.softmax,

                update_learning_rate=theano.shared(float32(0.01)),
                update_momentum=theano.shared(float32(0.9)),

                on_epoch_finished = [ 
                    AdjustVariable('update_learning_rate', start=0.01, stop=0.00001, adjust_type='expdecay'),
                    #AdjustVariable('update_momentum', start=0.9, stop=0.999),
                    model_saver,
                    ],

                max_epochs=self._num_epochs,
                batch_iterator_train=batch_iterator,
                batch_iterator_test=batch_iterator,
                #batch_iterator_train = LmdbBatchIterator(batch_size=128),
                #batch_iterator_train = MirrorBatchIterator(batch_size=256),
                verbose=1,
                eval_size=0.2,
                **self._kwargs
                )

        elif self._net_arch == "simple":
            # Simple net for test purposes
            #batch_iterator = LmdbBatchIterator(batch_size=256, lmdb_path=self._train_lmdb_path)
            net = NeuralNet(
                loss = multinomial_nll,
                layers=[
                    ('input', layers.InputLayer),
                    ('hidden1', layers.DenseLayer),
                    ('output', layers.DenseLayer),
                    ],
                input_shape=(None, self._num_channels, self._num_pixels, self._num_pixels),

                hidden1_num_units=512,
                output_num_units=self._num_classes,
                output_nonlinearity = nonlinearities.softmax,

                update = rmsprop,
                update_learning_rate=theano.shared(float32(0.0004)), #rmsprop

                on_epoch_finished = [model_saver],
                max_epochs=self._num_epochs,
                batch_iterator_train=batch_iterator,
                batch_iterator_test=batch_iterator,
                verbose=1,
                eval_size=0.2
                )

        else:
            raise Exception("Unknown network architecture")

        return net

    def fit(self, X, y, X_mean):
        """

        :X: TODO
        :y: TODO
        :returns: TODO

        """
        self.X_mean = X_mean # To be saved along with model
        print self.X_mean.shape, self.X_mean.dtype
        self._net.fit(X,y)

    def load_weights_from(self, weights):
        """Pass through function to net.load_weights_from, but reinitializes
        the net

        :weights: 
        :returns: None

        """
        self._net.load_weights_from(weights)
        self._net._initialized = False
        self._net.initialize()

    def predict_proba(self, X):
        """Pass through function
        :X: test data
        :returns: predictions

        """
        return self._net.predict_proba(X)

def float32(v):
    return np.cast['float32'](v)

class AdjustVariable(object):
    def __init__(self, name, start=0.03, stop=0.001, gamma=0.1, adjust_type='linear'):
        self.name = name
        self.start, self.stop = start, stop
        self.ls = None
        self.gamma = gamma
        self.adjust_type = adjust_type

    def __call__(self, nn, train_history):
        if self.ls is None:
            if self.adjust_type == 'linear':
                self.ls = np.linspace(self.start, self.stop, nn.max_epochs)
            elif self.adjust_type == 'expdecay':
                #self.ls = self.start * np.power(self.gamma, np.arange(0,nn.max_epochs,step=0.01))
                self.ls = self.start * np.power(self.gamma, np.arange(nn.max_epochs)//20)
            else:
                raise Exception("Unknown adjust_type")
            self.ls = np.clip(self.ls, self.stop, self.start)

        epoch = train_history[-1]['epoch']
        new_value = float32(self.ls[epoch - 1])
        print "new {}: {}".format(self.name, new_value)
        getattr(nn, self.name).set_value(new_value)


class LmdbBatchIterator(BatchIterator):

    """Loads images in batches from lmdb database"""

    def __init__(self, batch_size, lmdb_path, transform_cb=None):
        """

        :batch_size: TODO
        :lmdb_path: TODO
        :transform_cb: A function to call after internally transforming data

        """
        super(LmdbBatchIterator, self).__init__(batch_size)

        self.X, self.y = None, None
        self.lmdb_path = lmdb_path
        self.transform_cb = transform_cb

    def __iter__(self):
        """Load images from lmdb
        :returns: TODO

        """
        n_samples = self.X.shape[0]
        bs = self.batch_size

        env = lmdb.open(self.lmdb_path, map_size=1e9, lock=False)
        with env.begin(buffers=True) as txn:
            cursor = txn.cursor()
            cursor.first() #
            # Read the first element to get data shape
            d = Datum()
            d.ParseFromString(cursor.value())

            num_batches = (n_samples + bs - 1) // bs
            for i in range(num_batches):
                # Initialize Xb for speed

                # Get a slice that rapresents the current batch
                sl = slice(i * bs, (i + 1) * bs)
                X_keys = self.X[sl]
                Xb = np.zeros((len(X_keys),d.channels,d.height,d.width), dtype=np.float32)

                for k,key in enumerate(X_keys):
                    value = txn.get(key)
                    d.ParseFromString(value)
                    Xb[k] = np.fromstring(d.data, dtype=np.uint8).reshape(d.channels,d.height, d.width).astype(np.float32)/255.0

                if self.y is not None:
                    yb = self.y[sl]
                else:
                    yb = None

                print "Batch {} {:.2f}%     \r".format(i, (i*100.0)/num_batches),
                sys.stdout.flush()

                yield self.transform(Xb,yb)

    def transform(self, Xb, yb):
        """Call any transformers from the super class and the transform
        call back if available

        :Xb: TODO
        :yb: TODO
        :returns: TODO

        """
        
        Xb, yb = super(LmdbBatchIterator, self).transform(Xb, yb)
        if self.transform_cb is not None:
            Xb, yb = self.transform_cb(Xb, yb)
        return Xb, yb
 

         
class MirrorBatchIterator(LmdbBatchIterator):

    """Randomly mirrors some images in the batch"""

    def transform(self, Xb, yb):
        Xb, yb = super(MirrorBatchIterator, self).transform(Xb, yb)

        bs = Xb.shape[0]
        indices = np.random.choice(bs, bs/2, replace=False)
        Xb[indices] = Xb[indices, :, :, ::-1]
        #Xb = Xb - X_mean

        return Xb, yb

class NetModel(object):

    """Class that abstracts a model to train and predict on. It is yet another
    abstraction layer that makes it convenient to call train and predict and
    save the necessary model information to pickle files"""


    @classmethod
    def training(cls, *args, **kwargs):
        """Creates a trainer version of the class. Once created, it can be used
        to train a network.
        eg:
            model = NetModel.training('cxxnet', 'AllData_48x48_lmdb', 121,'models/simple')
            model.train()
            
        """
        obj = cls()
        obj._create_trainer(*args, **kwargs)
        return obj

    def _create_trainer(self, net_arch, train_lmdb_path, num_classes, output_prefix, num_pixels=96, **kwargs ):
        """Create neural net
        
        :net_arch: Network architecture (cxxnet, jay, msegala)
        :train_lmdb_path: lmdb database to training data. Should be in the dataset directory
        :num_classes: Number of classes (121 for full dataset)
        :output_prefix: Where to save the model
        :num_pixels: number of pixels of training/test dataset (default: 96)
        :kwargs: additional key word arguments that are passed to the NeuralNet class.
                These arguments can be used to modify hyper parameters of the net
                (Not fully implemented yet)
        """

        # Neural net architecture name should be the same for prediction and test
        self.net = NeuralNetWrapper(net_arch,
                num_classes=num_classes,
                num_pixels=num_pixels,
                output_prefix=output_prefix,
                train_lmdb_path=train_lmdb_path,
                **kwargs)

        self._num_pixels = num_pixels
        self._num_classes = num_classes
        self._output_prefix = output_prefix

        self.X_test = None
        self.X_train = None
        self.train_lmdb_path = train_lmdb_path

        self._num_channels = 1 # Hardcoded value (grayscale)

    def _load_train_lmdb(self, lmdb_path):
        X,y,_,X_mean = load_lmdb(lmdb_path, keys=True)
        # Randomize data
        rnd_ind = np.random.permutation(len(y))
        X = X[rnd_ind]
        y = y[rnd_ind]
        return X,y,X_mean

    def _load_preprocess_train(self, lmdb_path):
        print "Loading training data"
        self.X_train, self.y_train, self.X_mean = self._load_train_lmdb(lmdb_path)

        # We can't do this if we are loading images incrementally
        #self.X_train = (self.X_train - self.X_mean)
    
    def train(self):
        """This function loads images from the given lmdb_path and trains a neural
        net with the given number of output classes. The model is then saved in the
        output_prefix
        :returns: TODO

        """
        self._load_preprocess_train(self.train_lmdb_path)
        self.net.fit(self.X_train,self.y_train,self.X_mean)


    @classmethod
    def prediction(cls, test_lmdb_path='TestData_lmdb'):
        """Creates an object and loads the test dataset.
        
        Once created, it can be used to load multiple models and run
        predictions.
        eg:
            model = NetModel.prediction()
            output = model.predict('models/simple_1.pkl')
            model.save_to_csv('pred.csv', output)

        """

        obj = cls()
        obj._load_preprocess_test(test_lmdb_path)
        return obj

    def _load_preprocess_test(self, test_lmdb_path):
        print "Load test",
        self.test_lmdb_path = test_lmdb_path
        self.X_test, _, self.test_img_names,_ = load_lmdb(test_lmdb_path, keys=True)
        return self.X_test, self.test_img_names

    def _predict_using_weights(self, model, batch_iterator):
        """Given a model file, reconstruct the net, load the weights in and run
        predictions as opposed to using pickled functions. The pickled function
        is preferred and this method might get deprecated soon."""

        # Subtract the image mean used in training. This is used cxxnet and
        # improves performance in general
        X_mean = model['mean_image']

        self.net = NeuralNetWrapper(model['net_arch'],
                num_classes=model['classes'],
                num_pixels=model['pixels'],
                output_prefix='',
                train_lmdb_path='')

        # Hack!!! when the network is created, it gets its own batch iterator
        # so we have to replace it here with the batch iterator with the test
        # data
        self.net._net.batch_iterator_test = batch_iterator
        self.net.load_weights_from(model['best_weights'])

        print "\nRunning prediction"
        y_pred = self.net.predict_proba(self.X_test)
        print "\nDone"
        
        return y_pred

    def _predict_using_func(self, model, batch_iterator):
        """Given a model file, use the pickled function inside the model to run
         predictions"""


        predict_func = model['best_predict_func']

        print "\nRunning prediction"
        probas = []
        for Xb, yb in batch_iterator(self.X_test):
            probas.append(predict_func(Xb))

        print "\nDone"
        y_pred = np.vstack(probas)
        
        return y_pred

    def predict(self, model_file):
        """Given a model file, run prediction on all test images and return the
        class probabilities

        :model_file: model file in pkl format
        :returns: class probabilities

        """
        with open(model_file) as f:
            model = pickle.load(f)

        # Subtract the image mean used in training. This is used cxxnet and
        # improves performance in general
        X_mean = model['mean_image']
        def subtract_mean(Xb,yb):
            return Xb - X_mean, yb

        batch_size = 512
        batch_iterator = LmdbBatchIterator(batch_size=batch_size,
                                           lmdb_path=self.test_lmdb_path,
                                           transform_cb=subtract_mean)

        y_pred = self._predict_using_func(model, batch_iterator)

        #y_class = np.argmax(y_pred, axis=1)
        #print "Classes"
        #for o in zip(self.test_img_names, y_class):
        #    print o
        
        return zip(self.test_img_names,y_pred)

    def save_to_csv(self, out_path, data):
        print "Saving to csv {}".format(out_path)
        with open(out_path,'w') as f:
            csvout = csv.writer(f, delimiter=',')
            csvout.writerow(['image'] + csv_header)
            for img, pred in data:
                csvout.writerow([img] + pred.tolist())
