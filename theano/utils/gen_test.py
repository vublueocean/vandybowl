import os
import sys
import subprocess
from os.path import join

if len(sys.argv) < 3:
    print "Usage: python gen_test.py input_folder output_folder"
    exit(1)

fi = os.path.abspath(sys.argv[1])
fo = os.path.abspath(sys.argv[2])

cmd = "convert -resize 48x48\! "
imgs = os.listdir(fi)

try:
    os.mkdir(fo)
except:
    pass

for img in imgs:
    md = ""
    md += cmd
    md += join(fi, img)
    md += " " + join(fo, img)
    os.system(md)



