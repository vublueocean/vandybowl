import os
import sys
import subprocess
from os.path import join

if len(sys.argv) < 3:
    print "Usage: python gen_train.py input_folder output_folder"
    exit(1)

fi = os.path.abspath(sys.argv[1])
fo = os.path.abspath(sys.argv[2])


cmd = "convert -resize 48x48\! "
classes = os.listdir(fi)

try:
    os.mkdir(fo)
except:
    pass

os.chdir(fo)

for cls in classes:
    try:
        os.mkdir(cls)
    except:
        pass
    imgs = os.listdir(join(fi, cls))
    for img in imgs:
        md = ""
        md += cmd
        md += join(fi, cls, img)
        md += " " + join(fo, cls, img)
        os.system(md)



