## Tree Trainer
Tree trainer takes a yaml file representing the tree and will allow you to train a classifier at each node where the classifier classifies the input into one of the given branches. Each branch is the union of all descendent leaf nodes. 

Currently, the `generated_tree.yaml` tree uses the alphabetical index of each class as the identifier. Also, the training of the classifier needs to be added to the `train_on_node` method in `on_each_node.py`. `train_on_node` will provide the classifier with two arguments: _name_ and _branches_. _Name_ is the name of the given node in the tree. _Branches_ is a dictionary where the key is the name of the respective child node and the value is an array of all the classes represented by the given key.

## Nodes
Each node of the tree can also be found in `./nodes/` for convenience (in yaml format)

## Quick Start

### Training on entire tree
Add your training code to `on_each_node.py`:
```
import my_custom_trainer

def train_on_node(name, branches):
    for branch_name in branches:
        my_custom_trainer.train(branches[branch_name])
        my_custom_trainer.save_to_file("trained_model_for_"+name)

```
Then run
```
python2 tree_trainer.py
```

### Training on single node
Nodes are already created and exported as yaml files. They can be easily imported with the following code:

```
    with open(node_file_name, 'r') as f:
        node = yaml.load(f);
```

where `node_file_name` is the filename of the desired node (stored in `./nodes/`)
