'use strict';

var fs = require('fs'),
    classArray = require('./classes.js'),
    filename = 'tree.yaml';

fs.readFile(filename, 'utf-8', function(err, data) {
    var lines = data.split('\n'),
        wordRegex = /[\w-_]+/,
        j,
        word;

    for (var i = lines.length; i--;) {
        
        if (wordRegex.exec(lines[i])) {
            word = wordRegex.exec(lines[i])[0];
            console.log('word is '+word);
            if ((j = classArray.indexOf(word)) > -1) {
                lines[i] += ' '+j;
            }
        }
    }

    fs.writeFile('generated_'+filename, lines.join('\n'), function(err) {
        if (err) {
            console.error('Could not create file: '+err);
        } else {
            console.log('File has been created!');
        }
    });
});

