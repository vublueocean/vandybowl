#! /bin/bash/python2

"""
This script creates a tree.yaml file from a directory structure.
"""
import sys
import yaml
from os import listdir
from os.path import isdir, join, basename

if len(sys.argv) < 2:
    print("Usage: python2 tree_creator.py <directory> <output-filename>");
    exit(1);

# Use DFS to create the desired yaml
root_file  = sys.argv[1];
save_file = 'tree.yaml';

if len(sys.argv) > 2: 
    save_file = sys.argv[2];

def create_node(real_path):
    path = real_path;
    node = {};
    node_value = [];

    # Get the child directories
    child_dirs = get_child_dirs(path);

    # "Compress" nodes with only one child
    while len(child_dirs) == 1:
        path = child_dirs[0];
        child_dirs = get_child_dirs(path);

    if len(child_dirs) == 0:
        # If no child dirs, get the current path
        node_value = path;
    else:
        # Else, get an array of "createNode" of each child
        for child in child_dirs:
            node_value.append(create_node(child));

    node[basename(real_path)] = node_value;
    return node;

def get_child_dirs(dir_name):
    return [join(dir_name,f) for f in listdir(dir_name) if isdir(join(dir_name,f))];

tree = create_node(root_file);

print('Saving tree at '+save_file);
output_file = open(save_file, 'w');
yaml.dump(tree, output_file, default_flow_style=False);
output_file.close();
