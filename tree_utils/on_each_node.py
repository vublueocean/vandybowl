import sys
import os

# add our theano folder into path
theano_dir = os.path.abspath('../theano')
sys.path.append(theano_dir)

import yaml;
from neural_net_wrapper import NetModel

# This will be called when the tree_trainer.py is run
def on_every_node(name, branches):
    save_node_as_yaml(name, branches);
    # Uncomment the following to train the theano net on every node in the 
    # given tree
    #train_on_node(name, branches);

def save_node_as_yaml(name, branches):
    save_file = open(name+'.yaml', 'w');
    yaml.dump(branches, save_file, default_flow_style=False);

def train_on_node(name, branches):
    print("Training on node "+name+". Branches:");
    #print(name+' has the following branches: '+str(branches));

    lmdb_path = os.path.join(lasagnenet.dataset_dir,'lmdb', name+"_lmdb")
    num_classes = len(branches)
    num_pixels = 96 # Assume 96 for now
    output_prefix = os.path.join(theano_dir, 'models', name)

    print "LMDB:", lmdb_path
    print "Num clases", num_classes
    print "Classes", branches.keys()
    print "Output", output_prefix

    model = NetModel.training(net_arch='simple',
                              lmdb_path=lmdb_path,
                              num_classes=num_classes,
                              output_prefix=output_prefix,
                              num_pixels=num_pixels)
    model.train()

