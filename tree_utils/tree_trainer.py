#! /bin/env python

"""
This script takes the provided tree structure (in yaml) and trains a 
model at each node using the branches as classes.
"""

"""
To run code at each node, add code to "on_each_node.py"
"""

import yaml
import sys
import on_each_node

if len(sys.argv) < 2:
    print("Usage: python tree_trainer.py <tree-file.yaml>");
    exit(1);

def visit_node(name, node):

    if is_leaf_node(node):
        return get_images_from_leaf(node);  # Return the dir of the images

    # get the images at each branch
    elements = []
    itemsByBranch = {};
    for branch_name in node:
        branch = node[branch_name];
        child_elements = visit_node(branch_name, branch);
        itemsByBranch[branch_name] = child_elements;
        elements += child_elements;

    # train the model on the given elements
    train_model_on(name, itemsByBranch);
    # do something with the model TODO

    return elements;  # all images in the subtree

def get_branch_name(node):
    return node.keys()[0];

def is_leaf_node(node):
    # Check if the node contains a dir path
    return not type(node) is dict;

def get_images_from_leaf(node):
    return [node];

def train_model_on(name, branches):
    on_each_node.on_every_node(name, branches);
    return;
   
# Load the tree from yaml file
tree_file = sys.argv[1];
with open(tree_file, 'r') as f:
    tree = yaml.load(f);

# DFS on the tree and train when backing up
root_name = tree.keys()[0];

images = visit_node(root_name, tree[root_name]);

print('total image count '+str(len(images)));
